/*
 * FactoryI.h
 *
 *  Created on: Apr 19, 2015
 *      Author: zt
 */

#ifndef FACTORYI_H_
#define FACTORYI_H_
#include <Chat.h>


namespace Chat{
class ChatRoomFactoryI : virtual public ChatRoomFactory
{
private:
	std::vector<std::string> roomNames;
public:
	ChatRoomFactoryI();
    virtual ::Chat::ChatRoomPrx createRoom(const ::std::string&,
                                           const Ice::Current&);
};
}



#endif /* FACTORYI_H_ */
