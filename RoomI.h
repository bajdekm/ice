/*
 * RoomI.h
 *
 *  Created on: Apr 19, 2015
 *      Author: zt
 */

#ifndef ROOMI_H_
#define ROOMI_H_

#include <Chat.h>
namespace Chat {
class ChatRoomI: virtual public ChatRoom {
private:
	std::string name;
	RegisteredUsers users;
	Messages messages;
	IceUtil::Mutex _mutex;
public:
	ChatRoomI(const std::string&);
	virtual void addUser(const ::std::string&, const ::Chat::ChatUserPrx&,
			const Ice::Current&);

	virtual void sendMessage(const ::std::string&, const ::std::string&,
			const Ice::Current&);

	virtual void leaveRomm(const ::std::string&, const Ice::Current&);

	virtual ::Chat::LogedUsers showUsers(const Ice::Current&);

	virtual ::Chat::ChatUserPrx getChatUser(const ::std::string&,
			const Ice::Current&);
	virtual ::Chat::Messages getUpdates(const Ice::Current&);
	virtual void sendPrvMessage(const ::std::string&, const ::std::string&,
			const ::std::string&, const Ice::Current&);
};
}

#endif /* ROOMI_H_ */
