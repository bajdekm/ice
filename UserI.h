/*
 * UserI.h
 *
 *  Created on: Apr 19, 2015
 *      Author: zt
 */

#ifndef USERI_H_
#define USERI_H_

#include <Chat.h>

namespace Chat {

class ChatUserI: virtual public ChatUser {
private:
	std::string name;
	Messages messages;

	IceUtil::Mutex _mutex;
public:
	CallbackReceiverPrx receive;
	ChatUserI(const ::std::string &);
	virtual void sendMessage(const ::std::string&, const ::Chat::ChatRoomPrx&,
			const ::std::string&, const Ice::Current&);

	virtual void sendPrivateMessage(const ::std::string&,
			const ::Chat::ChatUserPrx&, const ::std::string&,
			const Ice::Current&);

	virtual void deliver(const ::std::string&, const Ice::Current&);

	virtual void initReceiver(const ::Chat::CallbackReceiverPrx&,
			const Ice::Current&);

	virtual void deleteUser(const Ice::Current&);

};

}

#endif /* USERI_H_ */
