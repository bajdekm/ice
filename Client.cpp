// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

#include <Ice/Ice.h>
#include <Chat.h>
#include <thread>
#include <mutex>
#include <CallbackReceiverI.h>

using namespace std;
using namespace Chat;

class Client {
private:
	//std::mutex mtx;
	Ice::CommunicatorPtr ic;
	//thread reader;
public:
	string name;
	string passwd;
	ChatUserPrx user;
	ChatRoomPrx room;
	ChatServerPrx server;
	CallbackReceiverPrx  receiver;
	bool isConnected = false;

	void setConnected(bool b) {
		//mtx.lock();
		isConnected = b;
		//mtx.unlock();
	}

	Client() {

		try {
			ic = Ice::initialize();
			Ice::ObjectPrx base = ic->stringToProxy(
					"MainServer:default -h localhost -p 10000");
			this->server = ChatServerPrx::checkedCast(base);
			if (!this->server) {
				throw "Invalid proxy";
			}

			 Ice::ObjectAdapterPtr adapter = ic->createObjectAdapterWithEndpoints("callbackReceiver", "default -h localhost");
			    CallbackReceiverPtr cr = new CallbackReceiverI;
			    adapter->add(cr, ic->stringToIdentity("callbackReceiver"));
			    adapter->activate();

			   this->receiver = CallbackReceiverPrx::uncheckedCast(
			        adapter->createProxy(ic->stringToIdentity("callbackReceiver")));


		} catch (const Ice::Exception& ex) {
			cerr << ex << endl;
			exit(-1);

		} catch (const char* msg) {
			cerr << msg << endl;
			exit(-2);
		}
	}
	~Client() {
		if (ic) {
			try {
				this->leaveRoom(this->name);
				this->logout(this->name, this->passwd);
				this->user->deleteUser();
				ic->destroy();
			} catch (const Ice::Exception& ex) {

			}
		}
	}
	void regiter(string user, string password) {
		try {
			server->Register(user, password);
		} catch (UserAlreadyExists& e) {
			cerr << "User already exist" << endl;
		}

		this->name = user;
		this->passwd = password;
	}

	void login(string login, string password) {
		try {
			this->user = server->Login(login, password);
			cout << "you're logged in" << endl;
			this->name = login;
			this->passwd = password;
			this->user->initReceiver(this->receiver);
		} catch (const LoginError& e) {
			cerr << "User not exists" << endl;
		} catch (const UserAlreadyLoggedIn& e) {
			cerr << "User already logged in" << endl;
		}
	}

	bool joinRoom(string name, string password) {
		try {
			this->room = this->server->joinRoom(name, this->name, this->user,
					this->passwd);
			cout << "you're joined room:" << name << endl;
			isConnected = true;
			return true;
			//
		} catch (NoSuchRoom & e) {
			cerr << "No such room" << endl;
			return false;
		} catch (UserAlreadyPresent & e) {
			return false;
		}
	}

	void createRoom(string name, string password) {
		try {
			this->room = this->server->createRoom(this->name, name, password);
			cout << "You create room:" << name << endl;
		} catch (RoomAlreadyExist &e) {
			cerr << "Room already exists" << endl;
		}
	}

	void sendMessage(string message) {
		this->room->sendMessage(message, this->name);
	}
	void sendPrvMessage(string message, string nick) {
		this->room->sendPrvMessage(message, nick, this->name);
	}

	void logout(string login, string pass) {
		try {
			this->server->Logout(login, pass);
			cout << "You're logged out";
		} catch (NoSuchUserLoggedIn & e) {
			cerr << "No such user" << endl;
		}
	}
	void showRooms() {
		RoomsInfo rooms = this->server->showRooms();
		for (RoomInfo r : rooms) {
			cout << r.name << " | ";
		}
		cout << endl;
	}
	void leaveRoom(string name) {
		this->room->leaveRomm(name);
	}

	void menu() {
		cout << "/quit /reg /login /logout /cRoom /jRoom /sRoom /say" << endl;
                cout << "Jak używać: /reg --> /login --> /cRoom lub /sRoom --> /jRoom i dopiero wysylac wiadomosci" << endl;

	}

	void controller(string c) {
	}

}
;

int main(int argc, char * argv[]) {
	std::ios_base::sync_with_stdio(false);

	Client client;
	client.menu();
	string c;
	if (argc > 1) {
		client.regiter(argv[1], "zt");
		client.login(argv[1], "zt");
		client.createRoom("room", "pass");
		client.joinRoom("room", "pass");
	}

	string login, pass, msg;
	 while (1){
		 getline(cin, c);

		if (c == "/login") {

			cout << "Login:";
			cin >> login;
			cout << "Password :";
			cin >> pass;
			client.login(login, pass);
		} else if (c == "/reg") {
			cout << "Login:";
			cin >> login;
			cout << "Password :";
			cin >> pass;
			client.regiter(login, pass);
		} else if (c == "/logout") {
			if (client.name.empty()) {
				cout << "Login:";
				cin >> login;
				cout << "Password :";
				cin >> pass;
			} else {
				login = client.name;
				pass = client.passwd;
			}
			client.logout(login, pass);
		} else if (c == "/cRoom") {
			cout << "Name:";
			cin >> login;
			cout << "Password :";
			cin >> pass;
			client.createRoom(login, pass);
		} else if (c == "/sRoom") {

			client.showRooms();
		} else if (c == "/lRoom") {
			cout << "Name:";
			cin >> login;
			client.leaveRoom(login);

		} else if (c == "/jRoom") {
			cout << "Login:";
			cin >> login;
			cout << "Password :";
			cin >> pass;
			client.joinRoom(login, pass);

		} else if (c == "/say") {
			cout << "Msg:";
			cin >> msg;
			client.sendMessage(msg);
		} else if (c == "/sayprv") {
			cout << "User:";
			cin >> login;
			cout << "Msg:";
			cin >> msg;
			client.sendPrvMessage(msg, login);

		} else if (c == "/quit") {
			return 0;
		}
	}
	//reader.join();
	return 0;
}
