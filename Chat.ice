module Chat {
	
	interface ChatUser;
	interface ChatRoom;
	interface ChatRoomFactory;
	interface ChatServer;
	interface CallbackReceiver;
	
	exception  UserAlreadyPresent{};
	exception NoSuchUser{};
	exception RoomAlreadyExist{};
	exception UserAlreadyExists{};
	exception UserAlreadyLoggedIn{};
	exception NoSuchUserLoggedIn{};
	exception NoSuchRoom{};
	exception AlreadyRegistered{};
	exception NoSuchFactory{};
	exception LoginError{};
	
	sequence<string> Messages;
	struct User{
		string name;
		string password;
		ChatUser * prx;
	};	
	struct LogedUser{
		User u;
	};
	sequence<LogedUser> LogedUsers;
	sequence<User> RegisteredUsers;
	
	interface CallbackReceiver {
		void deliver(string message);
	};
	
	interface  ChatUser {
		void sendMessage(string message,ChatRoom * proxy,string nick);
		void sendPrivateMessage(string message,ChatUser* who,string nick);
		void deliver(string message);
		void initReceiver(CallbackReceiver * receiver);
		void deleteUser();
	};
	
	interface ChatRoom {
		void addUser(string nick,ChatUser* who) throws UserAlreadyPresent;
		void sendMessage(string message, string nick);
		void sendPrvMessage(string message,string nick,string who);
		void leaveRomm(string nick);
		LogedUsers showUsers();
		ChatUser*  getChatUser(string nick) throws NoSuchUser;
		Messages getUpdates();
		
	};
	interface ChatRoomFactory {
		ChatRoom* createRoom(string nazwa) throws RoomAlreadyExist;
	};
	
	struct RoomInfo{
		string name;
		string password;
		string who;
		ChatRoom * prx;
		
	};
	struct FactoryInfo{
		string name;
		ChatRoomFactory * prx;
	};
	sequence<RoomInfo> RoomsInfo;
	sequence<FactoryInfo> FactoriesInfo;
	
	interface ChatServer {

		void Register(string nick, string passwd) throws UserAlreadyExists;
		ChatUser* Login(string nick, string passwd) throws UserAlreadyLoggedIn, LoginError;
		void Logout(string nick, string passwd) throws NoSuchUserLoggedIn;
		RoomsInfo showRooms();
		ChatRoom* joinRoom(string nazwa,string nick, ChatUser* who, string password) throws NoSuchRoom;
		ChatRoom* createRoom(string who, string nazwa, string password) throws RoomAlreadyExist;
		void registerFactory(ChatRoomFactory* who, string cookie) throws AlreadyRegistered;
		void unregisterFactory(ChatRoomFactory* who, string cookie) throws NoSuchFactory;
	};
	

	
	
};
