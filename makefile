all: Chat.cpp Chat.h Chat.o  client ServerI.o server Factory.o RoomI.o factory UserI.o CallbackReceiverI.o

Chat.cpp Chat.h : Chat.ice
	slice2cpp Chat.ice 
Chat.o : Chat.cpp
	g++ -c -I.  -m64 -Wall -Werror -pthread -fPIC -g -std=c++11 Chat.cpp
Client.o : Client.cpp
	g++ -c -I.  -m64 -Wall -Werror -pthread -fPIC -g -std=c++11 Client.cpp 
ServerI.o : ServerI.cpp ServerI.h
	g++ -c -I.  -m64 -Wall -Werror -pthread -fPIC -g -std=c++11  ServerI.cpp
Server.o : Server.cpp
	g++ -c -I.  -m64 -Wall -Werror -pthread -fPIC -g -std=c++11 Server.cpp 
RoomI.o : RoomI.cpp RoomI.h
		g++ -c -I.  -m64 -Wall -Werror -pthread -fPIC -g -std=c++11  RoomI.cpp
UserI.o : UserI.cpp UserI.h CallbackReceiverI.o
	g++ -c -I.  -m64 -Wall -Werror -pthread -fPIC -g -std=c++11  UserI.cpp 
CallbackReceiverI.o : CallbackReceiver.cpp CallbackReceiverI.h
	g++ -c -I.  -m64 -Wall -Werror -pthread -fPIC -g -std=c++11  CallbackReceiver.cpp
FactoryI.o: FactoryI.cpp FactoryI.h
	g++ -c -I.  -m64 -Wall -Werror -pthread -fPIC -g -std=c++11  FactoryI.cpp
Factory.o : Factory.cpp
	g++ -c -I.  -m64 -Wall -Werror -pthread -fPIC -g -std=c++11 Factory.cpp
factory:Chat.o Factory.o FactoryI.o RoomI.o
		g++ -m64 -Wall -Werror -pthread -fPIC -g  -Wl,--enable-new-dtags -rdynamic -m64 -Wall -Werror -pthread -fPIC -std=c++11\
 -g -o factory Chat.o Factory.o FactoryI.o RoomI.o -lIce -lIceUtil
client : Chat.o Client.o UserI.o CallbackReceiverI.o
	g++ -m64 -Wall -Werror -pthread -fPIC -g  -Wl,--enable-new-dtags -rdynamic -m64 -Wall -Werror -pthread -fPIC -std=c++11 -g\
 -o client Chat.o Client.o UserI.o CallbackReceiverI.o -lIce -lIceUtil

server : Chat.o Server.o ServerI.o  UserI.o 
	g++ -m64 -Wall -Werror -pthread -fPIC -g  -Wl,--enable-new-dtags -rdynamic -m64 -Wall -Werror -pthread -fPIC -std=c++11\
 -g -o server Chat.o Server.o ServerI.o UserI.o -lIce -lIceUtil

clean:
	rm client Client.o server Server.o Chat.o Chat.h Chat.cpp FactoryI.o Factory.o factory RoomI.o ServerI.o