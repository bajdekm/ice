
#include <RoomI.h>
Chat::ChatRoomI::ChatRoomI(const std::string & name){
	this->name = name;
	this->users = RegisteredUsers();
	this->messages = Messages();
}
void
Chat::ChatRoomI::addUser(const ::std::string& nick,
                         const ::Chat::ChatUserPrx& who,
                         const Ice::Current& current)
{
	IceUtil::Mutex::Lock lock(_mutex);  // Lock a mutex

	for (User user : users){
		if (user.name == nick){
			UserAlreadyPresent e;
			throw e;
			return;
		}
	}

	User user;
	user.name = nick;
	user.prx = who;
	//user.receiver = r;
	users.push_back(user);
}

void
Chat::ChatRoomI::sendMessage(const ::std::string& message,
                             const ::std::string& nick,
                             const Ice::Current& current)
{

	IceUtil::Mutex::Lock lock(_mutex);  // Lock a mutex

	for (User u :users){
		u.prx->deliver(message);

	}
	//messages.push_back(nick+":"+message);

}

void
Chat::ChatRoomI::leaveRomm(const ::std::string& nick,
                           const Ice::Current& current)
{
	IceUtil::Mutex::Lock lock(_mutex);  // Lock a mutex

	for (std::vector<User>::iterator it = users.begin();it!=users.end();++it){
		User & u = *it;
		if (u.name == nick){
			users.erase(it);
			return;
		}
	}

}

void
Chat::ChatRoomI::sendPrvMessage(const ::std::string& message,
                                const ::std::string& nick,
                                const ::std::string& who,
                                const Ice::Current& current)
{
	for (User u : users){
		if (u.name == nick){
			u.prx->deliver("[private]"+who+" : "+message);
		}
	}
}

::Chat::LogedUsers
Chat::ChatRoomI::showUsers(const Ice::Current& current)
{
    return ::Chat::LogedUsers();
}

::Chat::ChatUserPrx
Chat::ChatRoomI::getChatUser(const ::std::string& nick,
                             const Ice::Current& current)
{
    return 0;
}

::Chat::Messages
Chat::ChatRoomI::getUpdates(const Ice::Current& current)
{
	IceUtil::Mutex::Lock lock(_mutex);  // Lock a mutex
	Messages m = messages;
    return m;
}
