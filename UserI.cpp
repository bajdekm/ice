/*
 * UserI.cpp
 *
 *  Created on: Apr 19, 2015
 *      Author: zt
 */


#include <UserI.h>
#include <Ice/Ice.h>



Chat::ChatUserI::ChatUserI(const ::std::string & name){
	this->name = name;
	this->messages = Messages();
}
void
Chat::ChatUserI::sendMessage(const ::std::string& message,
                             const ::Chat::ChatRoomPrx& proxy,
                             const ::std::string& nick,
                             const Ice::Current& current)
{

	proxy->sendMessage(message,nick);
}

void
Chat::ChatUserI::sendPrivateMessage(const ::std::string& message,
                                    const ::Chat::ChatUserPrx& who,
                                    const ::std::string& nick,
                                    const Ice::Current& current)
{
}

void
Chat::ChatUserI::deleteUser(const Ice::Current& current)
{
	 try {
	        current.adapter->remove(current.id);
	    } catch (const Ice::NotRegisteredException&){
	        throw Ice::ObjectNotExistException(__FILE__, __LINE__);
	    }

}

void
Chat::ChatUserI::deliver(const ::std::string& message,
                         const Ice::Current& current)
{
	receive->deliver(message);
}

void Chat::ChatUserI::initReceiver(const ::Chat::CallbackReceiverPrx& receiver,
			const Ice::Current& current){
	receive  = receiver;
}


