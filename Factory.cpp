#include <Ice/Ice.h>
#include <Chat.h>
#include <FactoryI.h>
#include <RoomI.h>
using namespace std;
using namespace Chat;

int
main(int argc, char* argv[])
{
    int status = 0;
    Ice::CommunicatorPtr ic;
    try
    {
    	ic = Ice::initialize(argc, argv);
    	Ice::ObjectAdapterPtr adapter =  ic->createObjectAdapterWithEndpoints("FactoryAdapter", "default -h localhost -p 10001");
    	Ice::ObjectPtr object = new ChatRoomFactoryI;
    	adapter->add(object, ic->stringToIdentity("Factory"));
    	adapter->activate();
    	Ice::ObjectPrx base = ic->stringToProxy("MainServer:default -h localhost -p 10000");
    	ChatServerPrx server = ChatServerPrx::checkedCast(base);
    	if (!server){
    		return 0;
    	}
    	server->registerFactory(ChatRoomFactoryPrx::checkedCast(ic->stringToProxy("Factory:default -h localhost -p 10001")),"coockies");
    	ic->waitForShutdown();


    }
    catch(const Ice::Exception& e)
    {
        cerr << e << endl;
        status = 1;
    }
    catch(const char* msg)
    {
        cerr << msg << endl;
        status = 1;
    }
    if(ic)
    {
        try
        {
            ic->destroy();
        }
        catch(const Ice::Exception& e)
        {
            cerr << e << endl;
            status = 1;
        }
    }
    return status;
}


