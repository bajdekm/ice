#ifndef SERVERI_H_
#define SERVERI_H_
#include <Chat.h>
#include <IceUtil/Mutex.h>

namespace Chat {
class ChatServerI: virtual public ChatServer {
private:
	LogedUsers logedUsers;
	RegisteredUsers registeredUsers;
	RoomsInfo roomsInfo;
	FactoriesInfo factories;
	IceUtil::Mutex _mutex;
public:
	ChatServerI();
	virtual void Register(const ::std::string&, const ::std::string&,
			const Ice::Current&);

	::Chat::ChatUserPrx
	Login(const ::std::string&, const ::std::string&, const Ice::Current&);

	virtual void Logout(const ::std::string&, const ::std::string&,
			const Ice::Current&);

	virtual ::Chat::RoomsInfo showRooms(const Ice::Current&);

	virtual ::Chat::ChatRoomPrx joinRoom(const ::std::string&,
			const ::std::string&, const ::Chat::ChatUserPrx&,
			const ::std::string&, const Ice::Current&);

	virtual ::Chat::ChatRoomPrx createRoom(const ::std::string&,
			const ::std::string&, const ::std::string&, const Ice::Current&);

	virtual void registerFactory(const ::Chat::ChatRoomFactoryPrx&,
			const ::std::string&, const Ice::Current&);

	virtual void unregisterFactory(const ::Chat::ChatRoomFactoryPrx&,
			const ::std::string&, const Ice::Current&);
};
}

#endif /* SERVERI_H_ */
