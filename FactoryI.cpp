/*
 * FactoryI.cpp
 *
 *  Created on: Apr 19, 2015
 *      Author: zt
 */

#include <Ice/Ice.h>
#include <FactoryI.h>
#include <RoomI.h>

Chat::ChatRoomFactoryI::ChatRoomFactoryI(){
	this->roomNames = std::vector<std::string>();
}
::Chat::ChatRoomPrx
Chat::ChatRoomFactoryI::createRoom(const ::std::string& nazwa,
                                   const Ice::Current& current)
{
   try {
	   Ice::CommunicatorPtr comm = current.adapter->getCommunicator();
	   ChatRoomPtr ptr = new ChatRoomI(nazwa);

	   return ChatRoomPrx::checkedCast(current.adapter->add(ptr,comm->stringToIdentity(nazwa)));

   }
   catch ( Ice::AlreadyRegisteredException& e){
	   RoomAlreadyExist ee;
	   throw ee;
   }
   return 0;
}
