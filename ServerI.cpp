#include <ServerI.h>
#include <Ice/Ice.h>
#include <UserI.h>
Chat::ChatServerI::ChatServerI() {
	this->logedUsers = LogedUsers();
	this->registeredUsers = RegisteredUsers();
	this->factories = FactoriesInfo();
	this->roomsInfo = RoomsInfo();
	//IceUtil::Mutex(_fileMutex);
}

void Chat::ChatServerI::Register(const ::std::string& nick,
		const ::std::string& passwd, const Ice::Current& current) {
	IceUtil::Mutex::Lock lock(_mutex);  // Lock a mutex

	for (User user : this->registeredUsers) {
		if (user.name == nick) {
			UserAlreadyExists e;
			throw e;
		}
	}
	User user;
	user.name = nick;
	user.password = passwd;
	this->registeredUsers.push_back(user);
}

::Chat::ChatUserPrx Chat::ChatServerI::Login(const ::std::string& nick,
		const ::std::string& passwd, const Ice::Current& current) {
	IceUtil::Mutex::Lock lock(_mutex);  // Lock a mutex

	for (User user : this->registeredUsers) {
		if (user.name == nick && user.password == passwd) {
			try {
				Ice::CommunicatorPtr comm = current.adapter->getCommunicator();
				ChatUserPtr ptr = new ChatUserI(nick);
				ChatUserPrx prx = ChatUserPrx::checkedCast(
						current.adapter->add(ptr,
								comm->stringToIdentity(nick)));
				return prx;
			} catch (Ice::AlreadyRegisteredException& e) {
				UserAlreadyLoggedIn ee;
				throw ee;
			}
		}
	}
	/*	LogedUser lUser;
	 lUser.u.name = nick;
	 lUser.u.password = passwd;

	 this->logedUsers.push_back(lUser);*/

	/*}
	 }*/
	LoginError e;
	throw e;
}

void Chat::ChatServerI::Logout(const ::std::string& nick,
		const ::std::string& passwd, const Ice::Current& current) {
	IceUtil::Mutex::Lock lock(_mutex);  // Lock a mutex
	for (User user : this->registeredUsers) {
		if (user.name == nick && user.password == passwd) {
			//current.adapter.pr
			try {
				current.adapter->remove(
						current.adapter->getCommunicator()->stringToIdentity(
								nick));
			} catch (const Ice::NotRegisteredException&) {
				NoSuchUserLoggedIn e;
				throw e;
			}
		}
	}
}

::Chat::RoomsInfo Chat::ChatServerI::showRooms(const Ice::Current& current) {
	IceUtil::Mutex::Lock lock(_mutex);  // Lock a mutex
	RoomsInfo r = roomsInfo;
	return r;
}

::Chat::ChatRoomPrx Chat::ChatServerI::joinRoom(const ::std::string& nazwa,
		const ::std::string& nick, const ::Chat::ChatUserPrx& who,
		const ::std::string& password, const Ice::Current& current) {
	IceUtil::Mutex::Lock lock(_mutex);  // Lock a mutex
	for (RoomInfo room : roomsInfo) {
		if (room.name == nazwa) {
			room.prx->addUser(nick, who);
			return room.prx;
		}
	}
	throw new NoSuchRoom();
}

::Chat::ChatRoomPrx Chat::ChatServerI::createRoom(const ::std::string& who,
		const ::std::string& nazwa, const ::std::string& password,
		const Ice::Current& current) {
	IceUtil::Mutex::Lock lock(_mutex);  // Lock a mutex

	if (factories.size()) {
		FactoryInfo f = factories[0];
		ChatRoomPrx prx;
		try {
			prx = f.prx->createRoom(nazwa);
			RoomInfo room;
			room.name = nazwa;
			room.password = password;
			room.prx = prx;
			room.who = who;
			this->roomsInfo.push_back(room);
			return prx;
		} catch (RoomAlreadyExist& e) {
			throw e;
			return prx;
		}
	}
	else {
		NoSuchFactory e;
		throw e;
	}
}

void Chat::ChatServerI::registerFactory(const ::Chat::ChatRoomFactoryPrx& who,
		const ::std::string& cookie, const Ice::Current& current) {
	IceUtil::Mutex::Lock lock(_mutex);  // Lock a mutex

	for (FactoryInfo info : this->factories) {
		if (info.name == cookie) {
			AlreadyRegistered e;
			throw e;
		}
	}
	FactoryInfo info;
	info.name = cookie;
	info.prx = who;
	this->factories.push_back(info);
}

void Chat::ChatServerI::unregisterFactory(const ::Chat::ChatRoomFactoryPrx& who,
		const ::std::string& cookie, const Ice::Current& current) {
	IceUtil::Mutex::Lock lock(_mutex);  // Lock a mutex

	for (std::vector<FactoryInfo>::iterator it = factories.begin();
			it != factories.end(); ++it) {
		FactoryInfo & f = *it;
		if (f.name == cookie) {
			factories.erase(it);
			return;
		}
	}
	NoSuchFactory e;
	throw e;
}
