/*
 * CallbackReceiverI.cpp
 *
 *  Created on: Apr 24, 2015
 *      Author: zt
 */


#include <CallbackReceiverI.h>

void
Chat::CallbackReceiverI::deliver(const ::std::string& message,
                                 const Ice::Current& current)
{
#ifdef __xlC__
        //
        // The xlC compiler synchronizes cin and cout; to see the messages
        // while accepting input through cin, we have to print the messages
        // with printf
        //
        printf("%s\n",message.);
        fflush(0);
#else
        std::cout << message<< std::endl;
#endif
    }


