/*
 * CallbackReceiverI.h
 *
 *  Created on: Apr 24, 2015
 *      Author: zt
 */

#ifndef CALLBACKRECEIVERI_H_
#define CALLBACKRECEIVERI_H_

#include <Chat.h>

namespace Chat
{

class CallbackReceiverI : virtual public CallbackReceiver
{
public:

    virtual void deliver(const ::std::string&,
                         const Ice::Current&);
};
}



#endif /* CALLBACKRECEIVERI_H_ */
