// **********************************************************************
//
// Copyright (c) 2003-2013 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

#include <Ice/Ice.h>
#include <Chat.h>
#include <ServerI.h>
using namespace std;
using namespace Chat;

int
main(int argc, char* argv[])
{
    int status = 0;
    Ice::CommunicatorPtr ic;
    try
    {
    	ic = Ice::initialize(argc, argv);
    	Ice::ObjectAdapterPtr adapter = ic->createObjectAdapterWithEndpoints("MainServerAdapter", "default -h localhost -p 10000");
    	Ice::ObjectPtr object = new ChatServerI;
    	adapter->add(object, ic->stringToIdentity("MainServer"));
    	adapter->activate();

        ic->waitForShutdown();
    }
    catch(const Ice::Exception& e)
    {
        cerr << e << endl;
        status = 1;
    }
    catch(const char* msg)
    {
        cerr << msg << endl;
        status = 1;
    }
    if(ic)
    {
        try
        {
            ic->destroy();
        }
        catch(const Ice::Exception& e)
        {
            cerr << e << endl;
            status = 1;
        }
    }
    return status;
}
